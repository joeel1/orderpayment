const { Machine }      = require('xstate');
const { STATE_STATUS } = require('../constants'); 

const lightMachine = Machine({
  id: 'order',
  initial: STATE_STATUS.draft,
  states: {
    draft: {
        on: {
            CREATE: STATE_STATUS.created,
            CANCEL: STATE_STATUS.cancelled
        }
    },
    created: {
        on: {
            PAYMENT_RECEIVED: STATE_STATUS.confirmed,
            PAYMENT_DECLINED: STATE_STATUS.cancelled,
            CANCEL:           STATE_STATUS.cancelled
        }
    },
    confirmed: {
        after: {
            5000: STATE_STATUS.delivered
        }
    },
    delivered: {
        type: 'final',
    },
    cancelled: {
        type: 'final'
    }
  }
});

module.exports = lightMachine;