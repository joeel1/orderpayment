
const { interpret } = require('xstate');
const states        = require('./states');

const service = interpret(states)
    .onTransition(state => {
        console.log(`test - state: ${state.value}`);
    })
    .start();

console.log(`test - current: ${service.state.value}`);
let result = service.send('CREATE');
console.log(`\ntest - current: ${JSON.stringify(result, null, "    ")}`);
// service.send('PAYMENT_DECLINED');
// console.log(`test - current: ${service.state.value}`);
result = service.send('PAYMENT_RECEIVED');
console.log(`\ntest - current: ${JSON.stringify(result, null, "    ")}`);
setTimeout(() => {
    console.log(`\ntest - current - after 6 secs: ${JSON.stringify(service.state, null, "    ")}`);
}, 6000);

let res = states.transition('created', 'PAYMENT_RECEIVED', { dat: { dat1: "111", dat2: "222" } });
console.log(`test - trans: ${res.value}`);
console.log(`test - current: ${service.state.value}`);