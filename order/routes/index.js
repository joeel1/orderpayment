const express       = require('express');
const fetch         = require('node-fetch');
const { interpret } = require('xstate');
const router        = express.Router();
const { STATE_STATUS, STATE_ACTION, PAYMENT_STATUS, DOCUMENT_TYPE } = require('../../constants');
const { storeDocument, requestOrder }                               = require('../../commons');
const { updateState, logTransaction }                               = require('../logic');
const states                                                        = require('../states');

let counter   = 0;

router.post('/draft', function(req, res) {
    let body = req.body;

    counter         += 1;
    body._id         = `${DOCUMENT_TYPE.order}:${counter}`;
    body.type        = DOCUMENT_TYPE.order;
    body.status      = states.initialState.value;
    body.requestTime = new Date().getTime();

    store(res, body);
});

router.put('/:id', function (req, res) {
    var id = req.params.id;
    processOrder(res, id);
});

router.get('/:id', function (req, res) {
    var id     = req.params.id;
    var single = req.query.single;
    if (!single)
        request(res, id);
    else
        requestSingle(res, id);
});

router.delete('/:id', function (req, res) {
    var id = req.params.id;
    cancelOrder(res, id, STATE_ACTION.CUSTOMER_CANCEL);
});

/* API manual. */
router.get('/', function (req, res) {
    var str  = "REST interface:\n\n"
    str     += "\n"
    res.setHeader("content-type", "text/plain");
    res.end(str);
});

async function store(res, order) {
    try {
        let ret = await storeDocument(order);
        res.json(ret);
    } catch (err) { 
        res.json(err);
    }
    res.end();
}

async function processOrder(res, orderid) {
    let ret       = {};
    let newStatus = { value: STATE_STATUS.INVALID_STATE };
    let payment   = "";
    let order     = await requestOrder(orderid, true);
                    console.log(`[order] process(${orderid}): ${JSON.stringify(order, null, "    ")}`);
    if (order && order.status && order.status == STATE_STATUS.draft) {
                    console.log(`[order] process(${orderid}): ${JSON.stringify(order, null, "    ")}`);
            const service = interpret(states)
            .onTransition(state => {
                if (state.value == STATE_STATUS.delivered) {
                    newStatus = state;
                    console.log(`[order] process(${orderid}): ${state.value}`);
                    updateOrderStatus(orderid, state.value);
                }
            })
            .start();
        
        newStatus = service.send(STATE_ACTION.CREATE);
        await updateOrderStatus(orderid, newStatus.value);
        let responseFetch = await fetch ("http://127.0.0.1:3002/tac/" + order._id, { method: "POST" });
        let responseJson  = await responseFetch.json();
        if (responseJson && responseJson.result) {
            payment   = responseJson.result;
            newStatus = service.send(payment);
            await logTransaction(orderid, payment);
            await updateOrderStatus(orderid, newStatus.value, payment == PAYMENT_STATUS.PAYMENT_RECEIVED ? null : { note: payment });
        }
    }

    res.json({ status: newStatus.value, payment });
    res.end();
    return ret;
}

async function cancelOrder(res, orderid, note) {
    let order = await requestOrder(orderid, true);
    let ret   = {};
    if (order) {
        ret = await updateState(order, STATE_STATUS.cancelled, { note });
    }
    res.json(ret);
    res.end();
}

async function request(res, id) {
    let ret = await requestOrder(id);
    res.json(ret);
    res.end();
}

async function requestSingle(res, id) {
    let ret = await requestOrder(id, true);
    res.json(ret);
    res.end();
}

async function updateOrderStatus(id, new_status, extras) {
    let ret = await requestOrder(id, true);
    if (ret) {
        await updateState(ret, new_status, extras);
    }
}

module.exports = router;
