
const { storeDocument } = require('../commons');
const { DOCUMENT_TYPE } = require('../constants');

let counter = 0;


async function updateState(order, new_state, extras) {
    counter     += 1;
    order.status = new_state;
    extras       = extras || {};
    let json     = {
        _id: order._id + `:${DOCUMENT_TYPE.state}:` + counter,
        orderid:  order._id,
        type:     DOCUMENT_TYPE.state,
        status:   new_state,
        datetime: new Date().getTime(),
        ...extras
    };
    return await storeDocument([order, json]);
}

async function logTransaction(orderid, result) {
    counter += 1;
    let json = {
        _id: orderid + `:${DOCUMENT_TYPE.payment}:` + counter,
        orderid,
        type: DOCUMENT_TYPE.payment,
        result,
        datetime: new Date().getTime()
    };
    return await storeDocument(json);
}



module.exports.updateState    = updateState;
module.exports.logTransaction = logTransaction;
