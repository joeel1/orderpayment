const PouchDB = require('pouchdb-node');
const db      = new PouchDB("orderpayment");

async function storeDocument(document) {
    let ret = {};
    if (Array.isArray(document))
        ret = await db.bulkDocs(document);
    else
        ret = await db.put(document);
    return ret;

}

async function requestOrder(id, flagSingle) {
    let ret  = {};
    let opts = { include_docs: true };

    if (flagSingle) {
        opts.key = id;
    } else if (id && id.length > 0) {
        opts.start_key = id;
        opts.end_key   = id + "\ufff0";
    }

    let doc  = await db.allDocs(opts);
    if (doc && doc.rows && doc.rows.length > 0) {
        for (let row of doc.rows) {
            ret[row.key] = row.doc;
        }
    }
    if (flagSingle)
        return ret[id];
    else
        return ret;
}

module.exports.requestOrder       = requestOrder;
module.exports.storeDocument      = storeDocument;
