# Order Management System

There are 2 services created to cater Order Management System
* `order` : micro service which manage orders
* `payment` : micro service which simulate payment gateway

## Framework
* `ExpressJS` : web framework - to provide REST endpoints
* `PouchDB` : embedded `document based` NoSQL DB
    * on production, the NoSQL DB should run stand alone, preferably by server such as `CouchDB`
* `xstate` : State Engine framework
    * on production, custom state engine may be required.

to install
```javascript
    $ git clone
    
    $ npm install
```

____
# Payment Service
* simulate payment gateway server
* running on port `3002`
* only `process` payment for order with status `created`

* run service:
```javascript
    $ npm run payment
```

## REST interface
* parameters passed in as url path: `http://127.0.0.1:3002/<tac>/<orderid>`
    * `method`  : POST
    * `tac`     : dummy TAC - not being used
    * `orderid` : id for order document, created earlier

* http request will return JSON as result
```javascript
{
    "result": "<result>"
}
```

* `result`:
    * `INVALID_ORDERID`  :  order id passed in doesnt exist
    * `INVALID_STATE`    :  order not in `created` status
    * `PAYMENT_RECEIVED` : payment `approved` by bank
    * `PAYMENT_DECLINED` : payment `declined` by bank


# Order Service
* order's states is managed by `State Engine (xstate)`
* to manage orders
* run on port `3001`

* run service:
```javascript
    $ npm run order
```


## REST interface
* create order `draft`
    * `url`: http://127.0.0.1:3001/draft
    * `method`: POST
    * `body`: order's properties, passed in JSON format
```javascript
        {
	        "title": "some order",
	        "item": "some items",
	        "quantity": 5
        }
```

* `result`
```javascript
        // take note on `id`
        // will be used in next transactions
        {
            "ok": true,
            "id": "order:9",
            "rev": "1-5bb0efe749629d006dc95e1de147da93"
        }
```

* process payment for order
    * will move order through states, defined by `State Engine`
    * final state, depending on `Payment Status`
    * `url`: http://127.0.0.1:3001/`<orderid>`
    * `method`: PUT
    * `result`
```javascript
        {
            "status": "INVALID_STATE",
            "payment": ""
        }
```

* fetching order's details
    * fecth any documents, related to that specific order, `ie`: order, state's log, payment log
    * `url`: http://127.0.0.1:3001/`<orderid>`
    * `method`: GET
    * `result`
```javascript
        {
            "order:8": {
                "title": "some order",
                "item": "some items",
                "quantity": 5,
                "type": "order",
                "status": "delivered",
                "requestTime": 1550672598405,
                "_id": "order:8",
                "_rev": "4-757c8b832b10de8b5b424188095964cd"
            },
            "order:8:payment:8": {
                "orderid": "order:8",
                "type": "payment",
                "result": "PAYMENT_RECEIVED",
                "datetime": 1550672613524,
                "_id": "order:8:payment:8",
                "_rev": "1-1307590920be782f4296eb9054248e59"
            },
            "order:8:state:10": {
                "orderid": "order:8",
                "type": "state",
                "status": "delivered",
                "datetime": 1550672618531,
                "_id": "order:8:state:10",
                "_rev": "1-eecc170212d15683e5dce3102ce83b17"
            },
            "order:8:state:7": {
                "orderid": "order:8",
                "type": "state",
                "status": "created",
                "datetime": 1550672613515,
                "_id": "order:8:state:7",
                "_rev": "1-0592d36f71d6a3fa143a3e08791ad621"
            },
            "order:8:state:9": {
                "orderid": "order:8",
                "type": "state",
                "status": "confirmed",
                "datetime": 1550672613526,
                "_id": "order:8:state:9",
                "_rev": "1-25d41ed533d05c179d1e42d8d70f6190"
            }
        }
```