
const fetch                            = require('node-fetch');
const { STATE_STATUS, PAYMENT_STATUS } = require('../constants');

async function processPayment(tac, orderid) {
    let ret = PAYMENT_STATUS.INVALID_ORDERID;
    if (orderid && orderid.length > 0) {
        try {
            let responseFetch = await fetch ("http://127.0.0.1:3001/" + orderid + "?single=true", { method: "GET" });
            let result        = await responseFetch.json();

            if (result) {
                if (result.status && result.status == STATE_STATUS.created) {
                    const dice     = Math.floor(Math.random() * Math.floor(2));
                    if (dice === 0) 
                        ret = PAYMENT_STATUS.PAYMENT_RECEIVED;
                    else
                        ret = PAYMENT_STATUS.PAYMENT_DECLINED;
                } else
                    ret = PAYMENT_STATUS.INVALID_STATE;
            }
        } catch (err) { console.log("error: " + err); return err; }
    }
    return ret;
}

module.exports.processPayment = processPayment;