const express            = require('express');
const { processPayment } = require('../logic');
const router             = express.Router();

router.post('/:dummytac/:orderid', function(req, res, next) {
    const dummytac = req.params.dummytac;
    const orderid  = req.params.orderid;
    
    processPaymentRequest(dummytac, orderid, res);
});

async function processPaymentRequest(tac, orderid, res) {
    let ret = { result: await processPayment(tac, orderid) };
    if (res) {
        res.json(ret);
        res.end();
    }
}

module.exports = router;
