
module.exports.STATE_STATUS = {
    draft:         'draft',
    created:       'created',
    confirmed:     'confirmed',
    delivered:     'delivered',
    cancelled:     'cancelled',
    INVALID_STATE: 'INVALID_STATE'
};

module.exports.STATE_ACTION = {
    CREATE:           'CREATE',
    CANCEL:           'CANCEL',
    PAYMENT_RECEIVED: 'PAYMENT_RECEIVED',
    PAYMENT_DECLINED: 'PAYMENT_DECLINED',
    CANCEL:           'CANCEL',
    CUSTOMER_CANCEL:  'CUSTOMER_CANCEL'
};

module.exports.PAYMENT_STATUS = {
    INVALID_ORDERID:  'INVALID_ORDERID',
    INVALID_STATE:    'INVALID_STATE',
    PAYMENT_RECEIVED: 'PAYMENT_RECEIVED',
    PAYMENT_DECLINED: 'PAYMENT_DECLINED'
};

module.exports.DOCUMENT_TYPE = {
    order:   'order',
    state:   'state',
    payment: 'payment'
};

