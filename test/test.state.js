let assert          = require('assert');
const { interpret } = require('xstate');
const states        = require('../order/states');


describe('Unit Test - State Engine', function() {

    describe('payment received flow', function() {
        const service = interpret(states)
            .onTransition(state => {})
            .start();

        it('initial state is "draft"', function() {
            assert.equal('draft', service.state.value);
        });

        it('move from "draft" to "created"', function() {
            assert.equal('created', service.send('CREATE').value);
        });

        it('payment received. state is now "confirmed"', function() {
            assert.equal('confirmed', service.send('PAYMENT_RECEIVED').value);
        });

        it('state still "confirmed"', function() {
        console.log('-> automatically move from "confirmed" to "delivered" after 5 seconds');
            assert.equal('confirmed', service.state.value);
        });

        it('wait for 6 seconds, before checking the status again...', function(done) {
            this.timeout(6500);
            setTimeout(() => {
                assert.equal('delivered', service.state.value);
                done();
            }, 6000);
        });
    });


    describe('\npayment declined flow', function() {
        const service = interpret(states)
            .onTransition(state => {})
            .start();

        it('initial state is "draft"', function() {
            assert.equal('draft', service.state.value);
        });

        it('move from "draft" to "created"', function() {
            assert.equal('created', service.send('CREATE').value);
        });

        it('declining payment. state is now "cancelled"', function() {
            assert.equal('cancelled', service.send('PAYMENT_DECLINED').value);
        });
    });


    describe('\ncancel order flow', function() {
        const service = interpret(states)
            .onTransition(state => {})
            .start();

        it('initial state is "draft"', function() {
            assert.equal('draft', service.state.value);
        });

        it('move from "draft" to "created"', function() {
            assert.equal('created', service.send('CREATE').value);
        });

        it('cancel order. state is now "cancelled"', function() {
            assert.equal('cancelled', service.send('CANCEL').value);
        });
    });
});
