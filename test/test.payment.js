let assert               = require('assert');
const { interpret }      = require('xstate');
const { storeDocument }  = require('../commons');
const { processPayment } = require('../payment/logic');



    let json1 = {
        _id: 'order:11',
        status: 'created'
    }
    let json2 = {
        _id: `order:12`,
        status: 'confirmed'
    }
    storeDocument([json1, json2]).then((storeRes) => {
        describe('Unit Test - Payment Logic', function() {
            it('making payment for "created" order', function(done) {
                processPayment('dummytac', 'order:11').then(result => {
                    let res = result == 'PAYMENT_RECEIVED' || result == 'PAYMENT_DECLINED';
                    assert.equal(true, res);
                    done();
                });
            });

            it('payment failed for "confirmed" order', function(done) {
                processPayment('dummytac', 'order:12').then(result => {
                    assert.equal('INVALID_STATE', result);
                    done();
                });
            });

            it('payment failed for non-existant order', function(done) {
                processPayment('dummytac', 'order:123').then(result => {
                    assert.equal('INVALID_ORDERID', result);
                    done();
                });
            });
        });
    });
